import pygame
import pygame_menu
from pygame_menu import themes

pygame.init()
screen = pygame.display.set_mode((800, 600))
pygame.display.set_caption("LinguaBattle")

background = (58, 42, 107)
background2 = (107, 2, 107)
button = (0, 255, 0)
check = (108, 22, 107)

big_font = pygame.font.Font("Flavius2008.ttf", 40)
main_font = pygame.font.Font("Flavius2008.ttf", 35)

def uniki_menu():
    mainmenu._open(uniki)

def item_selected():
    mainmenu.disable()
    screen.fill(check)
    pygame.display.update()

class Knopka:
    def __init__(self, text, font):
        self.text = text
        self.font = font

    def addKnopka(self, dest, color="White"):
        self.dest = dest
        self.color = color
        self.rect = pygame.draw.rect(screen, background2, [self.dest[0], self.dest[1], 120, 30])
        screen.blit(self.font.render(self.text, True, self.color), self.dest)

        pygame.display.flip()
        return self.rect

texttype = ""
inputting = False
numlist = (pygame.K_0, pygame.K_1, pygame.K_2, pygame.K_3, pygame.K_4, pygame.K_5, pygame.K_6, pygame.K_7, pygame.K_8, pygame.K_9)

program = True
while program:

    mainmenu = pygame_menu.Menu('LinguaBattle', 600, 400)
    mainmenu.add.button('Начать', uniki_menu)

    uniki = pygame_menu.Menu('Выберите уник:', 600, 400)
    uniki.add.button('ОТиПЛ МГУ', item_selected)#, pc_fak=1)
    uniki.add.button('ФиКЛ ВШЭ', item_selected)#, pc_fak=2)
    uniki.add.button('ФиПЛ РГГУ', item_selected)#, pc_fak=3)
    uniki.add.button('ПКиМЛ СПбГУ', item_selected)#, pc_fak=4)

    mainmenu.mainloop(screen)

    screen.fill(background)
    pygame.display.update()

    #unik = Knopka('униикккк', main_font)
    #unik.addKnopka((300, 300))

    def begin_round():
        cbegin.addKnopka((0, 10), "yellow")

        c1.addKnopka((0, 50))

        c2.addKnopka((0, 100))

        c3.addKnopka((0, 150))

        c4.addKnopka((0, 200))

        c5.addKnopka((0, 250))

        c6.addKnopka((0, 300))

        c7.addKnopka((0, 350))

        c8.addKnopka((0, 400))

        c9.addKnopka((0, 450))

        c10.addKnopka((0, 500))

        c11.addKnopka((0, 550))

    cbegin = Knopka('Выберите желаемое действие', big_font)

    c1 = Knopka('Обучение', main_font)

    c2 = Knopka('Поступление', main_font)

    c3 = Knopka('Пиар-кампания', main_font)

    c4 = Knopka('Найм преподавателей', main_font)

    c5 = Knopka('Закупка еды', main_font)

    c6 = Knopka('Подработка студентов', main_font)

    c7 = Knopka('Заявка на грант', main_font)

    c8 = Knopka('Лабораторная работа', main_font)

    c9 = Knopka('Время оплаты', main_font)

    c10 = Knopka('Отчисление', main_font)

    c11 = Knopka('Увольнение', main_font)

    begin_round()

    #choicedict = {c1: 1,
                  #c2: 2,
                  #c3: 3,
                  #c4: 4,
                 #c5: 5,
                  #c6: 6,
                  #c7: 7,
                  #c8: 8,
                  #c9: 9,
                  #c10: 10,
                  #c11: 11}

    #choiceset = {c1, c2, c3, c4, c5, c6, c7, c8, c9, c10, c11}

    while True:
        events = pygame.event.get()
        for event in events:
            if event.type == pygame.QUIT:
                pygame.quit()
            if event.type == pygame.MOUSEBUTTONDOWN and c1.rect.collidepoint(pygame.mouse.get_pos()):
                inputting = True
                screen.fill(background)
                howmuch = Knopka('Сколько? (вводите цифры и нажмите Enter)', main_font)
                howmuch.addKnopka((0, 100))
            if event.type == pygame.KEYDOWN and inputting is True:
                if event.key in numlist:
                    texttype += event.unicode
                    screen.blit(main_font.render(texttype, True, 'green'), (100, 200))
                    pygame.display.flip()
                if event.key == pygame.K_RETURN:
                    # записать в переменную
                    texttype = ""
                    inputting = False
                    screen.fill(background)
                    pygame.display.update()
                    begin_round()
                    #elif event.key
                        #texttype = ""
                        #screen.blit(main_font.render(texttype, True, 'green'), (100, 200))
                        #pygame.display.flip()
                #pygame.display.update()