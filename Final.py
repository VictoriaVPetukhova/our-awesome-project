from random import randint
import sys
import pygame
import pygame_menu
from pygame_menu import themes

global pc_fak
pc_fak = 0

global count
count = 1

global winner
winner = None

class Game:
    def __init__(self):
        otipl = Faculty(name="ОПеПл МемГУ", money=30, food=40, studs=50, teachs=60, edu_scale=50)  # 1
        fikl = Faculty(name="ХихиКЛ ВЩЕщь", money=50, food=30, studs=40, teachs=60, edu_scale=50)  # 2
        fipl = Faculty(name="ХихиПЛ РхехеГУ", money=40, food=50, studs=60, teachs=30, edu_scale=50)  # 3
        pkiml = Faculty(name="ПКикиМЛ СПеп-8ГУ", money=50, food=60, studs=30, teachs=40, edu_scale=50)  # 4
        self.faks = [otipl, fikl, fipl, pkiml]

    def run(self):
        winner = self.faks[1]
        while count < 11:
            choice = 0
            global n
            for n in range(4):
                if n != pc_fak - 1:
                    act = randint(1, 11)
                    self.faks[n].shag(act)
                else:
                    self.faks[n].shag(choice)
                    if (self.faks[n].money or self.faks[n].food or self.faks[n].studs or self.faks[n].teachs) < 0:
                        screen.fill('black')
                        screen.blit(main_font.render("Кажется, вы переборщили и разрушили свою карьеру. Нам жаль, но игра окончена :(", True, 'red'), (0, 300))
                        pygame.display.update()
                        sys.exit()
                    #print(self.faks[n]) #!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
            #print("Конец хода №", count, ". У вас осталось", 10 - count, "ходов.")
            #count += 1

class Faculty:
    def __init__(self, name: str, money: int, food: int, studs: int, teachs: int, edu_scale: int):
        self.name = name
        self.money = money
        self.food = food
        self.studs = studs
        self.teachs = teachs
        self.edu_scale = edu_scale
        self.rate = self.money*0.1 + self.food*0.1 + self.studs*0.3 + self.teachs*0.3 + self.edu_scale*0.5

    def __str__(self):
        return f"{self.name}\n\n" \
            f"Характеристики (мин. - 0, макс. - 100)\n" \
            f"Деньги: {self.money}\n" \
            f"Еда: {self.food}\n" \
            f"Студенты: {self.studs}\n" \
            f"Преподаватели: {self.teachs}\n" \
            f"Шкала образования: {self.edu_scale}\n" \
            f"Рейтинг вуза: {self.rate}"\


    def educate(self):  # 1
        self.edu_scale += self.studs

    def entry(self):  # 2
        if n != pc_fak - 1:
            postup = randint(5, 15)
        else:
            postup = None #int(input('Сколько? '))
            #while postup not in range(self.edu_scale):
                #print("Число не подходит:(")
                #postup = int(input())
        self.studs += postup

    def piar(self):  # 3
        if n != pc_fak - 1:
            buy = randint(20, 40)
        else:
            buy = None #int(input('Сколько? '))
        self.money -= buy
        self.studs += buy

    def naim(self):  # 4
        if n != pc_fak - 1:
            buy = randint(3, 19)
        else:
            buy = None #int(input('Сколько? '))
        self.money -= 5 * buy
        self.teachs += buy

    def feed(self):  # 5
        if n != pc_fak - 1:
            buy = randint(1, 10)
        else:
            buy = int(input('Сколько? '))
        self.money -= buy
        self.food += buy

    def job(self):
        if n != pc_fak - 1:
            work = randint(1, 2)
            zarp = randint(1, 10)
        else:
            print("Введите опцию подработки:\n 1 - Еда\n 2 - Работа")
            work = None #str(input())
            zarp = None #int(input())
        while zarp not in range(self.studs):
            zarp = int(input())
        if work == "1":
            self.money += zarp
            self.edu_scale -= zarp // 2
        if work == "2":
            self.food += zarp
            self.edu_scale -= zarp // 2

    def grant(self):  # 7
        self.money += self.teachs // 2
        self.edu_scale += self.teachs // 2

    def lab(self):  # 8
        self.edu_scale += self.teachs

    def pay(self):  # 9
        self.money += self.studs

    def kick(self):  # 10
        if n != pc_fak - 1:
            kicks = randint(1, 10)
        else:
            kicks = None #int(input('Сколько? '))
        self.studs -= kicks

    def fire(self):  # 11
        if n != pc_fak - 1:
            fires = randint(1, 10)
        else:
            fires = None #int(input('Сколько? '))
        self.teachs -= fires

    def shag(self, k):
        if k == 1:
            self.educate()
        elif k == 2:
            self.entry()
        elif k == 3:
            self.piar()
        elif k == 4:
            self.naim()
        elif k == 5:
            self.feed()
        elif k == 6:
            self.job()
        elif k == 7:
            self.grant()
        elif k == 8:
            self.lab()
        elif k == 9:
            self.pay()
        elif k == 10:
            self.kick()
        elif k == 11:
            self.fire()
        self.rate = self.money * 0.1 + self.food * 0.1 + self.studs * 0.3 + self.teachs * 0.3 + self.edu_scale * 0.5

pygame.init()
screen = pygame.display.set_mode((800, 600))
pygame.display.set_caption("LinguaBattle")

background = (58, 42, 107)
background2 = (107, 2, 107)
button = (0, 255, 0)
check = (108, 22, 107)

big_font = pygame.font.Font("Flavius2008.ttf", 35)
main_font = pygame.font.Font("Flavius2008.ttf", 20)

def uniki_menu():
    mainmenu._open(uniki)

def item_selected(name):
    pc_fak = int(name)
    mainmenu.disable()
    screen.fill(background)
    pygame.display.update()

def work_selected(nmbr):
    work = int(nmbr)
    workmenu.disable()
    screen.fill(background)
    pygame.display.update()

class Knopka:
    def __init__(self, text, font):
        self.text = text
        self.font = font

    def addKnopka(self, dest, color="White"):
        self.dest = dest
        self.color = color
        self.rect = pygame.draw.rect(screen, background2, [self.dest[0], self.dest[1], 120, 30])
        screen.blit(self.font.render(self.text, True, self.color), self.dest)

        pygame.display.flip()
        return self.rect

roundtext = str(f"Конец хода № {count}.\n У вас осталось {10 - count} ходов.")
texttype = ""
inputting = False
numlist = (pygame.K_0, pygame.K_1, pygame.K_2, pygame.K_3, pygame.K_4, pygame.K_5, pygame.K_6, pygame.K_7, pygame.K_8, pygame.K_9)

program = True
while program:

    mainmenu = pygame_menu.Menu('LinguaBattle', 600, 400)
    mainmenu.add.button('Начать', uniki_menu)

    uniki = pygame_menu.Menu('Выберите уник:', 600, 400)
    uniki.add.button('ОТиПЛ МГУ')
    uniki.add.button('ФиКЛ ВШЭ')
    uniki.add.button('ФиПЛ РГГУ')
    uniki.add.button('ПКиМЛ СПбГУ')
    uniki.add.text_input('уник:', maxchar=1, valid_chars=(['1', '2', '3', '4']), onreturn=item_selected)


    mainmenu.mainloop(screen)

    screen.fill(background)
    pygame.display.update()

    #unik = Knopka('униикккк', main_font)
    #unik.addKnopka((300, 300))

    def begin_round():
        if count < 11:
            screen.blit(main_font.render(roundtext, True, 'white'), (300, 300))
            pygame.display.flip()
        else:
            winner == Game.run.faks[1]
            screen.fill('orange')
            pygame.display.update()
            for n in range(4):
                # main.font.render(rate, True, "yellow")
                # print(self.faks[n].rate)
                if self.faks[n].rate > winner.rate:
                    winner = self.faks[n]
            if winner == self.faks[pc_fak - 1]:
                screen.blit(main_font.render("Вы выиграли!!!", True, 'yellow'), (0, 300))
                pygame.display.flip()
            else:
                screen.blit(main_font.render("Вы проиграли :(", True, 'blue'), (0, 300))
                pygame.display.flip()


        cbegin.addKnopka((0, 10), "yellow")

        c1.addKnopka((0, 50))

        c2.addKnopka((0, 100))

        c3.addKnopka((0, 150))

        c4.addKnopka((0, 200))

        c5.addKnopka((0, 250))

        c6.addKnopka((0, 300))

        c7.addKnopka((0, 350))

        c8.addKnopka((0, 400))

        c9.addKnopka((0, 450))

        c10.addKnopka((0, 500))

        c11.addKnopka((0, 550))

    cbegin = Knopka('Выберите желаемое действие', big_font)

    c1 = Knopka('Обучение', main_font)

    c2 = Knopka('Поступление', main_font)

    c3 = Knopka('Пиар-кампания', main_font)

    c4 = Knopka('Найм преподавателей', main_font)

    c5 = Knopka('Закупка еды', main_font)

    c6 = Knopka('Подработка студентов', main_font)

    c7 = Knopka('Заявка на грант', main_font)

    c8 = Knopka('Лабораторная работа', main_font)

    c9 = Knopka('Время оплаты', main_font)

    c10 = Knopka('Отчисление', main_font)

    c11 = Knopka('Увольнение', main_font)

    begin_round()

    choicedict = {c1: 1,
                  c2: 2,
                  c3: 3,
                  c4: 4,
                  c5: 5,
                  c6: 6,
                  c7: 7,
                  c8: 8,
                  c9: 9,
                  c10: 10,
                  c11: 11}

    #choiceset = {c1, c2, c3, c4, c5, c6, c7, c8, c9, c10, c11}

    while True:
        events = pygame.event.get()
        for event in events:
            if event.type == pygame.QUIT:
                pygame.quit()
            if event.type == pygame.MOUSEBUTTONDOWN and c1.rect.collidepoint(pygame.mouse.get_pos()):
                choice = choicedict.get(c1)
                count += 1
                begin_round()

            if event.type == pygame.MOUSEBUTTONDOWN and c2.rect.collidepoint(pygame.mouse.get_pos()):
                choice = choicedict.get(c2)
                inputting = True
                screen.fill(background)
                howmuch = Knopka('Сколько? (вводите цифры и нажмите Enter)', main_font)
                howmuch.addKnopka((0, 100))
                count += 1

            if event.type == pygame.MOUSEBUTTONDOWN and c3.rect.collidepoint(pygame.mouse.get_pos()):
                choice = choicedict.get(c3)
                inputting = True
                screen.fill(background)
                howmuch = Knopka('Сколько? (вводите цифры и нажмите Enter)', main_font)
                howmuch.addKnopka((0, 100))
                count += 1
                

            if event.type == pygame.MOUSEBUTTONDOWN and c4.rect.collidepoint(pygame.mouse.get_pos()):
                choice = choicedict.get(c4)
                inputting = True
                screen.fill(background)
                howmuch = Knopka('Сколько? (вводите цифры и нажмите Enter)', main_font)
                howmuch.addKnopka((0, 100))
                count += 1

            if event.type == pygame.MOUSEBUTTONDOWN and c5.rect.collidepoint(pygame.mouse.get_pos()):
                choice = choicedict.get(c5)
                inputting = True
                screen.fill(background)
                howmuch = Knopka('Сколько? (вводите цифры и нажмите Enter)', main_font)
                howmuch.addKnopka((0, 100))
                count += 1

            if event.type == pygame.MOUSEBUTTONDOWN and c6.rect.collidepoint(pygame.mouse.get_pos()):
                choice = choicedict.get(c6)
                count += 1

                workmenu = pygame_menu.Menu('Выберите работу', 500, 500)
                workmenu.add.button('Еда')
                workmenu.add.button('Работа')
                workmenu.add.text_input('выбор (1/2):', maxchar=1, valid_chars=(['1', '2']), onreturn=work_selected)

                workmenu.mainloop(screen)

                inputting = True
                screen.fill(background)
                howmuch = Knopka('Сколько? (вводите цифры и нажмите Enter)', main_font)
                howmuch.addKnopka((0, 100))

            if event.type == pygame.MOUSEBUTTONDOWN and c7.rect.collidepoint(pygame.mouse.get_pos()):
                choice = choicedict.get(c7)
                count += 1
                begin_round()

            if event.type == pygame.MOUSEBUTTONDOWN and c8.rect.collidepoint(pygame.mouse.get_pos()):
                choice = choicedict.get(c8)
                count += 1
                begin_round()

            if event.type == pygame.MOUSEBUTTONDOWN and c9.rect.collidepoint(pygame.mouse.get_pos()):
                choice = choicedict.get(c9)
                count += 1
                begin_round()

            if event.type == pygame.MOUSEBUTTONDOWN and c10.rect.collidepoint(pygame.mouse.get_pos()):
                choice = choicedict.get(c10)
                inputting = True
                screen.fill(background)
                howmuch = Knopka('Сколько? (вводите цифры и нажмите Enter)', main_font)
                howmuch.addKnopka((0, 100))
                count += 1

            if event.type == pygame.MOUSEBUTTONDOWN and c11.rect.collidepoint(pygame.mouse.get_pos()):
                choice = choicedict.get(c11)
                inputting = True
                screen.fill(background)
                howmuch = Knopka('Сколько? (вводите цифры и нажмите Enter)', main_font)
                howmuch.addKnopka((0, 100))
                count += 1


            if event.type == pygame.KEYDOWN and inputting is True and choice == 2:
                if event.key in numlist:
                    texttype += event.unicode
                    screen.blit(main_font.render(texttype, True, 'green'), (100, 200))
                    pygame.display.flip()
                if event.key == pygame.K_RETURN:
                    if int(texttype) not in range(Faculty.edu_scale):
                        screen.blit(main_font.render("Число не подходит:(", True, 'red'), (500, 500))
                        pygame.display.flip()
                        texttype = ""
                    else:
                        postup = int(texttype)
                        texttype = ""
                        inputting = False
                        screen.fill(background)
                        pygame.display.update()
                        begin_round()

            if event.type == pygame.KEYDOWN and inputting is True and choice == 3:
                if event.key in numlist:
                    texttype += event.unicode
                    screen.blit(main_font.render(texttype, True, 'green'), (100, 200))
                    pygame.display.flip()
                if event.key == pygame.K_RETURN:
                    buy = int(texttype)
                    texttype = ""
                    inputting = False
                    screen.fill(background)
                    pygame.display.update()
                    begin_round()

            if event.type == pygame.KEYDOWN and inputting is True and choice == 4:
                if event.key in numlist:
                    texttype += event.unicode
                    screen.blit(main_font.render(texttype, True, 'green'), (100, 200))
                    pygame.display.flip()
                if event.key == pygame.K_RETURN:
                    buy = int(texttype)
                    texttype = ""
                    inputting = False
                    screen.fill(background)
                    pygame.display.update()
                    begin_round()

            if event.type == pygame.KEYDOWN and inputting is True and choice == 5:
                if event.key in numlist:
                    texttype += event.unicode
                    screen.blit(main_font.render(texttype, True, 'green'), (100, 200))
                    pygame.display.flip()
                if event.key == pygame.K_RETURN:
                    buy = int(texttype)
                    texttype = ""
                    inputting = False
                    screen.fill(background)
                    pygame.display.update()
                    begin_round()

            if event.type == pygame.KEYDOWN and inputting is True and choice == 6:
                if event.key in numlist:
                    texttype += event.unicode
                    screen.blit(main_font.render(texttype, True, 'green'), (100, 200))
                    pygame.display.flip()
                if event.key == pygame.K_RETURN:
                    zarp = int(texttype)
                    texttype = ""
                    inputting = False
                    screen.fill(background)
                    pygame.display.update()
                    begin_round()

            if event.type == pygame.KEYDOWN and inputting is True and choice == 10:
                if event.key in numlist:
                    texttype += event.unicode
                    screen.blit(main_font.render(texttype, True, 'green'), (100, 200))
                    pygame.display.flip()
                if event.key == pygame.K_RETURN:
                    kicks = int(texttype)
                    texttype = ""
                    inputting = False
                    screen.fill(background)
                    pygame.display.update()
                    begin_round()

            if event.type == pygame.KEYDOWN and inputting is True and choice == 11:
                if event.key in numlist:
                    texttype += event.unicode
                    screen.blit(main_font.render(texttype, True, 'green'), (100, 200))
                    pygame.display.flip()
                if event.key == pygame.K_RETURN:
                    fires = int(texttype)
                    texttype = ""
                    inputting = False
                    screen.fill(background)
                    pygame.display.update()
                    begin_round()

#g1 = Game()
#g1.run()
