from random import randint
import sys


class Game:
    def __init__(self):
        otipl = Faculty(name="ОПеПл МемГУ", money=30, food=40, studs=50, teachs=60, edu_scale=50)  # 1
        fikl = Faculty(name="ХихиКЛ ВЩЕщь", money=60, food=50, studs=30, teachs=40, edu_scale=50)  # 2
        fipl = Faculty(name="ХихиПЛ РхехеГУ", money=50, food=60, studs=40, teachs=30, edu_scale=50)  # 3
        pkiml = Faculty(name="ПКикиМЛ СПеп-8ГУ", money=40, food=30, studs=60, teachs=50, edu_scale=50)  # 4
        self.faks = [otipl, fikl, fipl, pkiml]

    def run(self):
        global pc_fak
        pc_fak = int(input(("Вам будут представленны на выбор несколько университетов, которыми можно управлять."
                            " Укажите номер того, за который вы хотите играть:"
                            "\n1 — ОПеПл МемГУ\n2 — ХихиКЛ ВЩЕщь\n3 — ХихиПЛ РхехеГУ\n4 — ПКикиМЛ СПеп-8ГУ\n")))
        while pc_fak not in [1, 2, 3, 4]:
            print("такого фалкультета нет, выберите ещё раз: ")
            pc_fak = int(input())
        winner = self.faks[1]
        count = 1
        while count < 11:
            choice = int(input("Введите номер желаемого действия:\n1 — Обучение\n2 — Поступление\n3 — Пиар-кампания\n"
                               "4 — Найм преподавателей\n5 — Закупка еды\n6 — Подработка студентов\n7 — Заявка на грант"
                               "\n8 — Лабораторная работа\n9 — Время оплаты\n10 — Отчисление\n11 — Увольнение\n"))
            while choice not in range(12):
                print("К сожалению, такой команды нет :(")
                choice = int(input())
            global n
            for n in range(4):
                if n != pc_fak - 1:
                    act = randint(1, 11)
                    self.faks[n].shag(act)
                else:
                    self.faks[n].shag(choice)
                    if (self.faks[n].money or self.faks[n].food or self.faks[n].studs or self.faks[n].teachs) < 0:
                        print("Кажется, вы переборщили и разрушили свою карьеру. Нам жаль, но игра окончена :(")
                        sys.exit()
                    print(self.faks[n])
            print("Конец хода №", count, ". У вас осталось", 10 - count, "ходов.")
            count += 1
        if count == 11:
            winner == self.faks[1]
            for n in range(4):
                print(self.faks[n].rate)
                if self.faks[n].rate > winner.rate:
                    winner = self.faks[n]
            if winner == self.faks[pc_fak-1]:
                print("Вы выиграли!!!")
            else:
                print("Вы проиграли(")


class Faculty:
    def __init__(self, name: str, money: int, food: int, studs: int, teachs: int, edu_scale: int):
        self.name = name
        self.money = money
        self.food = food
        self.studs = studs
        self.teachs = teachs
        self.edu_scale = edu_scale
        self.rate = self.money*0.1 + self.food*0.1 + self.studs*0.3 + self.teachs*0.3 + self.edu_scale*0.5

    def __str__(self):
        return f"{self.name}\n\n" \
            f"Характеристики (мин. - 0, макс. - 100)\n" \
            f"Деньги: {self.money}\n" \
            f"Еда: {self.food}\n" \
            f"Студенты: {self.studs}\n" \
            f"Преподаватели: {self.teachs}\n" \
            f"Шкала образования: {self.edu_scale}\n" \
            f"Рейтинг вуза: {self.rate}"\


    def educate(self):  # 1
        self.edu_scale += self.studs

    def entry(self):  # 2
        if n != pc_fak - 1:
            postup = randint(5, 15)
        else:
            postup = int(input('Сколько? '))
            while postup not in range(self.edu_scale):
                print("Число не подходит:(")
                postup = int(input())
        self.studs += postup

    def piar(self):  # 3
        if n != pc_fak - 1:
            buy = randint(20, 40)
        else:
            buy = int(input('Сколько? '))
        self.money -= buy
        self.studs += buy

    def naim(self):  # 4
        if n != pc_fak - 1:
            buy = randint(3, 19)
        else:
            buy = int(input('Сколько? '))
        self.money -= 5 * buy
        self.teachs += buy

    def feed(self):  # 5
        if n != pc_fak - 1:
            buy = randint(1, 10)
        else:
            buy = int(input('Сколько? '))
        self.money -= buy
        self.food += buy

    def job(self):
        if n != pc_fak - 1:
            work = randint(1, 2)
            zarp = randint(1, 10)
        else:
            print("Введите опцию подработки:\n 1 - Еда\n 2 - Работа")
            work = str(input())
            zarp = int(input())
        while zarp not in range(self.studs):
            zarp = int(input())
        if work == "1":
            self.money += zarp
            self.edu_scale -= zarp // 2
        if work == "2":
            self.food += zarp
            self.edu_scale -= zarp // 2

    def grant(self):  # 7
        self.money += self.teachs // 2
        self.edu_scale += self.teachs // 2

    def lab(self):  # 8
        self.edu_scale += self.teachs

    def pay(self):  # 9
        self.money += self.studs

    def kick(self):  # 10
        if n != pc_fak - 1:
            kicks = randint(1, 10)
        else:
            kicks = int(input('Сколько? '))
        self.studs -= kicks

    def fire(self):  # 11
        if n != pc_fak - 1:
            fires = randint(1, 10)
        else:
            fires = int(input('Сколько? '))
        self.teachs -= fires

    def shag(self, k):
        if k == 1:
            self.educate()
        elif k == 2:
            self.entry()
        elif k == 3:
            self.piar()
        elif k == 4:
            self.naim()
        elif k == 5:
            self.feed()
        elif k == 6:
            self.job()
        elif k == 7:
            self.grant()
        elif k == 8:
            self.lab()
        elif k == 9:
            self.pay()
        elif k == 10:
            self.kick()
        elif k == 11:
            self.fire()
        self.rate = self.money * 0.1 + self.food * 0.1 + self.studs * 0.3 + self.teachs * 0.3 + self.edu_scale * 0.5


g1 = Game()
g1.run()
